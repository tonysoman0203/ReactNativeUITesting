import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,TouchableHighlight,TouchableOpacity,
  Image,TextInput,
  Button,ScrollView,
  Picker,
  DatePickerAndroid,
  ToastAndroid,
  View,Platform,
} from 'react-native';
import moment from 'moment'
import { StackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
    lpsTitle: {
        color: '#000000',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 20
    }
});

class LoginContent extends Component {
    constructor(props){
        super(props)
        this.state = ({
            username: '',
            password: ''
        })
    }

    render(){
        return(
           <ScrollView> 
            <View style={{flex: 1, backgroundColor: "#FFFFFF"}}>
                <Text style={styles.lpsTitle}>Life Insurance Proposal System</Text>
                <View style={{width: 350, height: 150, backgroundColor: '#E8E8E8', justifyContent: 'center', alignSelf:'center'}}>
                    <View style={{width: 200, flexDirection: 'row'}}>
                        <Text style={{color: "#000000", marginLeft:20, alignSelf: 'center'}}>Username</Text>
                        <TextInput
                        ref={(e1) => { this.username = e1; }} 
                        underlineColorAndroid='transparent'
                        autoCorrect={false}
                        onChangeText={(username) => this.setState({username})}
                        style={{width: 200, height: 40, marginLeft: 50, borderColor: '#000000', 
                        borderRadius: 5,
                        borderWidth: 1, backgroundColor:'#FFFFFF'}}>
                        </TextInput>
                        
                    </View>  
                    <View style={{width: 200, flexDirection: 'row', marginTop: 20}}>    
                        <Text style={{color: "#000000", marginLeft:20, alignSelf: 'center'}}>Password</Text>
                    <TextInput 
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    ref={(e1) => { this.password = e1; }} 
                    secureTextEntry={true} 
                     onChangeText={(password) => this.setState({password})}
                    style={{width: 200, marginLeft: 55,height: 40,
                    borderRadius: 5,
                    borderColor: '#000000', borderWidth: 1, backgroundColor:'#FFFFFF'}}>
                 </TextInput>
                    </View>
                </View>
                <TouchableHighlight
                style={{width: 80, height: 50,alignSelf:'center', justifyContent:'center', marginTop:20}}
                onPress={this._onPress.bind(this)}
                >
                <View style={{width: 80, height: 50, backgroundColor:'#950D3F', alignSelf:'center', justifyContent:'center', borderRadius: 5}}>
                        <Text style={{color: '#FFFFFF', fontSize: 16, justifyContent:'flex-end', alignSelf:'center'}}>Login</Text>
                </View>
                </TouchableHighlight>
                
            </View>
            </ScrollView>
        );
    }

    _onPress(){
        if(Platform.OS == 'android'){
            ToastAndroid.show('A  pikachu appeared nearby ! username: '+this.state.username+' password: '+this.state.password, ToastAndroid.SHORT);
        }else{
            alert('A pikachu appeared nearby ! username: '+this.state.username+' password: '+this.state.password)
        }
    }
}

module.exports = LoginContent