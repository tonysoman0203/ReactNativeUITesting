import {StyleSheet} from 'react-native';
import Dimensions from 'react-native';

module.exports = StyleSheet.create({
    dslaSplashScreen: {
        width: '100%', 
        height:'100%'
    },
    lpsTitle: {
        fontSize: 14,
        color: '#FFFFFF',
        fontWeight:'bold'
    },
    lpsTitleContainer: {
        alignSelf: 'center',
        marginLeft: 20
    },
    lpsVersionNLangContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignSelf: 'center'
    },
    lpsVersionNLangText: {
        fontSize: 16,
        color: '#FFFFFF',
        marginRight: 20,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    lpsLangText: {
        fontSize: 16,
        color: '#FFFFFF',
        justifyContent: 'center',
        alignSelf: 'center',
        alignContent: 'center'
    },
    lpsLangButton: {
        width: 110, 
        height: 35, 
        justifyContent: 'center',
        marginRight: 20
    },
   webViewHeader: {
       alignSelf:'center',
       justifyContent:'center',
       color: '#000000',
   },
   webViewCloseBtn: {
       width:150,
       alignSelf:'flex-end',
       justifyContent: 'flex-end',
       alignItems:'flex-end'
   },
   modalContainer: {
       backgroundColor: 'transparent',
       flex: 1,
       marginTop:50,
       marginLeft: 200,
       marginRight: 200,
       borderRadius: 4, 
   },
underlineText: {
        textDecorationLine: 'underline',
        alignSelf:'center',
        marginLeft:10,
        paddingLeft:10, 
        color: '#000000',
        justifyContent: 'center',
        width: 140,
        fontSize: 16
    },
    divider: {
        backgroundColor: '#BDBDBD',
        width: 3,
        marginBottom: 7,
        marginTop: 7
    },
    
    
});