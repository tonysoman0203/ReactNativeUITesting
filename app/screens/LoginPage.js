import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,TouchableHighlight,TouchableOpacity,
  Image,
  Button,
  Picker,
  DatePickerAndroid,
  View
} from 'react-native';
import moment from 'moment'
import { StackNavigator } from 'react-navigation';
var HeaderScreen = require('./HeaderScreen.js');
var FooterScreen = require('./FooterScreen.js');
var LoginContent = require('./LoginContent.js')

var WebViewModal = require('./WebViewModal.js');

export default class LoginPage extends Component {
    static navigationOptions = {
      header: null // !!! Hide Header
    }
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={{flex: 1}}>
                <HeaderScreen></HeaderScreen>
                <LoginContent />
                <View style={{height:40, alignContent: 'flex-end'}}>
                    <FooterScreen />
                </View>
                
            </View>
        );
    }
}
