import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,TouchableHighlight,TouchableOpacity,
  Image,
  Button,
  Picker,
  DatePickerAndroid,
  View
} from 'react-native';
import moment from 'moment'
import { StackNavigator } from 'react-navigation';
var Constant = require('../utils/Constant.js');
var styles = require('./css/MyStyleSheet.js');

class HeaderScreen extends Component {
    static navigationOptions = {
      header: null // !!! Hide Header
    }
    constructor(props){
        super(props);
        this.state = {
            time: moment().format('HH:mm:ss'),
            date: moment().format('YYYY-MM-DD')
        }
    }
    render() {
        setTimeout(() => {
			this.setState({
				time: moment().format('HH:mm:ss'),
                date: moment().format('YYYY-MM-DD')
			});
        }, 1000);
        
        return(
          <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
            <View style={{justifyContent:'space-between', flexDirection: 'row'}}>
                <View style={{paddingLeft:20, paddingTop:20, alignItems: 'flex-start', flexDirection:'row'}}>
                    <Image style={{width: 200, alignSelf: 'flex-start'}}
                    resizeMode='contain'
                    source={Constant.photoPath.dslalogoLeft} />
                    <Image style={{width: 100, alignSelf:'flex-end'}}
                    resizeMode='contain' 
                    source={Constant.photoPath.dslalogo2} />
                </View>    
                <Image style={{width: 150, justifyContent:'flex-end', alignSelf: 'flex-end'}}
                   resizeMode='contain'
                    source={Constant.photoPath.dslalogoRight}
                    />    
            </View>
            <View style={{backgroundColor: '#950D3F', height: 50,justifyContent:'space-between', flexDirection:'row'}}>
                <View style={styles.lpsTitleContainer}>
                    <Text style={styles.lpsTitle}>Life Insurance Proposal System</Text> 
                </View>
                <View style={styles.lpsVersionNLangContainer}>
                        <Text style={styles.lpsVersionNLangText}>
                            {this.state.date} {this.state.time}
                        </Text> 
                        <Text style={styles.lpsVersionNLangText}>
                            Version : LPS16H04A
                        </Text>
                        <View style={{width: 110, height: 35, marginRight: 20}}>
                             <TouchableHighlight>
                                 <Image source={Constant.photoPath.bgLangButton} style={styles.lpsLangButton}>
                                    <Text style={styles.lpsLangText}>ENG</Text>
                                 </Image>
                             </TouchableHighlight>       
                        </View>
                    </View>    
            </View>        
          </View>
        );
    }
}

module.exports = HeaderScreen;