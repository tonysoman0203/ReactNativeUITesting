    import React, { Component } from 'react';
    import {
    AppRegistry,
    StyleSheet,
    Text,TouchableHighlight,TouchableOpacity,
    Image,
    Button,
    Picker,
    WebView,
    DatePickerAndroid,
    Modal,
    ToastAndroid,
    View
    } from 'react-native';
    import moment from 'moment'
    import { StackNavigator, NavigationActions } from 'react-navigation';
    import Constant from '../utils/Constant';
    var stylesheet = require('./css/MyStyleSheet.js')

    class WebViewModal extends Component {
        state = {
            fromItemName: '',
            isModalOpen: false,
            _resetModal: null
        }

        constructor(props){
            super(props)
            this.state = {
                fromItemName: this.props.fromItemName,
                isModalOpen: this.props.isModalOpen,
                _resetModal: this.props._resetModal
            }
            this._onCloseButtonClick = this._onCloseButtonClick.bind(this,false);
        }

        _onCloseButtonClick = (visible) => {
            this.setState({
                isModalOpen: visible
            })
            this.state._resetModal(false)

            ToastAndroid.show("Modal Close", ToastAndroid.SHORT)
        }
        
        render(){
            return(
                    <Modal
                        animationType='fade'
                        transparent={true}
                        backdropOpacity={.9}
                        presentationStyle={'pageSheet'}
                        visible={!!this.state.isModalOpen}
                        onShow={() => {}}
                        onRequestClose={()=>{this._onCloseButtonClick.bind(this, false); alert('Modal has been closed. ')}}
                        >
                        <View style={{opacity: 1, flex: 1, backgroundColor: '#000000'}}>
                        <View  style={stylesheet.modalContainer}>
                            <View style={{flexDirection: 'row', backgroundColor:'#BDBDBD', alignItems:'flex-start', alignContent:'flex-end', justifyContent:'flex-end'}}>

                            <View style={{alignSelf:'center', justifyContent:'center', flexDirection: 'row', flex:1}}>
                                <Text style={stylesheet.webViewHeader}>
                                    {this.state.fromItemName}
                                </Text>
                            </View>

                                <View style={stylesheet.webViewCloseBtn}>
                                    <Button
                                    onPress={this._onCloseButtonClick.bind(this, false)}
                                    color='#950D3F'
                                    title='close'
                                    />
                                </View>    
                            </View>
                            <WebView
                            source={this.state.fromItemName == 'terms'? Constant.URLs.termAndUseURL : 
                            this.state.fromItemName=='internet'? (Constant.URLs.internetURL) : Constant.URLs.privacyURL}
                            /> 
                        </View>
                        </View>    
                    </Modal>   
            );
        }
    }

    module.exports = WebViewModal;
