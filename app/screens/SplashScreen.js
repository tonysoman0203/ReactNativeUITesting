import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Image,
  View
} from 'react-native';
import moment from 'moment'
import { StackNavigator, NavigationActions } from 'react-navigation';

var Constant = require('../utils/Constant.js');
var myStyle = require ('./css/MyStyleSheet.js');

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'LoginPage'})
  ]
})

class SplashScreen extends Component {
    static navigationOptions = {
      header: null // !!! Hide Header
    }
    render(){
        setTimeout(() => {
            this.props.navigation.dispatch(resetAction);
        }, 3000);

        return(
            <View style={{flex: 1}}>
                <Image
                 style={myStyle.dslaSplashScreen}
                 source={Constant.photoPath.dslasplashLogo} />
            </View>    
        );
    }
}

module.exports = SplashScreen;