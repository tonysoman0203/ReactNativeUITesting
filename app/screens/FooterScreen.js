import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,TouchableHighlight,TouchableOpacity,
  Image,
  Button,
  ScrollView,
  Picker,
  DatePickerAndroid,ToastAndroid,
  View,Modal,WebView,
} from 'react-native';
import moment from 'moment'
import { StackNavigator } from 'react-navigation';
import WebViewModal from './WebViewModal';
import GeoFencingModal from './GeoFencingModal'

var styles = require ('./css/MyStyleSheet.js');

class FooterScreen extends Component{
    static navigationOptions = {
      header: null // !!! Hide Header
    }

    constructor(props){
        super(props)
        this.state = {
            fromItemName: '',
            isModalOpen: false,
            _resetModal: this._resetModal.bind(this, false),
            _onGeoFencingCall: this._onGeoFencingCall.bind(this, false)
        }
        
        this._OnTextViewClick = this._OnTextViewClick.bind(this,'');
    }

    render(){
        return(
           
            <View style={{flex: 1, flexDirection: 'row', alignContent: 'flex-end', backgroundColor:'#FAFAFA'}}>
                 <ScrollView horizontal={true}> 
                <Text 
                onPress={this._OnTextViewClick.bind(this,'terms')}
                style={styles.underlineText}>
                    Terms of Use
                </Text>
                <View style={styles.divider}></View>
                <Text onPress={this._OnTextViewClick.bind(this,'privacy')}
                 style={styles.underlineText}>
                    Privacy Policy
                </Text>
                <View style={styles.divider}></View>
                <Text onPress={this._OnTextViewClick.bind(this,'internet')} style={styles.underlineText}>
                    Internet Security
                </Text>
                <View style={styles.divider}></View>
                <Text style={styles.underlineText}>
                    Version
                </Text>
                  <View style={styles.divider}></View>
                <Text style={styles.underlineText} onPress={this._onGeoFencingCall.bind(this,true)}>
                    GeoFencing Page
                </Text>
                <View>
                    {this._renderWebView()}
                </View>
                
                <View>
                    {this._renderGeoFencingModal()}
                </View>
                 </ScrollView>
            </View>
           
        );
    }

    _OnTextViewClick = (props,itemname) => {
        this.setState({
            fromItemName: itemname,
            isModalOpen: true
        });
    }


    _onGeoFencingCall = (visible) => {
        this.setState({
            isGeoFencingModalCall: visible
        })

    }

    _renderWebView = () => {
        if(this.state.isModalOpen){
             ToastAndroid.show("_renderWebView isModalOpen"+this.state.isModalOpen, ToastAndroid.SHORT)
            return (
              <WebViewModal 
              isModalOpen={this.state.isModalOpen}
              fromItemName={this.state.fromItemName}
              _resetModal={this.state._resetModal}
              />  
            );
        }
    }

    _renderGeoFencingModal = () => {
        if(this.state.isGeoFencingModalCall){
            return (
                <GeoFencingModal
                    _onGeoFencingCall={this.state._onGeoFencingCall} 
                    isVisible={!!this.state.isGeoFencingModalCall}
                />
            );
        } 
    }

    _resetModal = (visible) => {
        this.setState({
            isModalOpen: visible,
        })
    }
}

module.exports = FooterScreen;