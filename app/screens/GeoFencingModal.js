import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,TouchableHighlight,TouchableOpacity,
  Image,
  Button,
  ScrollView,
  Picker,TextInput,
  DatePickerAndroid,ToastAndroid,
  View,Modal,WebView,
} from 'react-native';
import moment from 'moment'
import GeoFencing from 'react-native-geo-fencing'
const polygon = [
    { lat: 22.3102832, lng: 114.2212316},
    { lat: 22.309943, lng: 114.221626 },
    { lat: 22.3103213,  lng: 114.2223332 },
    { lat: 22.3105394, lng: 114.2208573 },
    { lat:22.3129376, lng: 114.2201792 },
    { lat: 22.3135568, lng: 114.2210735}, 
    { lat: 22.3102832, lng: 114.2212316 } // last point has to be same as first point
];

var geoFencingInterval = null;

class GeoFencingModal extends Component {
  
  state = {
    isVisible: false,
  }

  constructor(props){
    super(props)
    this.state = {
      distance: parseFloat(100),
      isVisible: this.props.isVisible,
      isGeoFencingModalCall: false,
      _onGeoFencingCall: this.props._onGeoFencingCall.bind(this),
      _onGeoFencingActivate: this._onGeoFencingActivate.bind(this),
            latitude: '',
            longitude:'',
            accuracy: '' ,
            jsonLog: '',
            error: null,
            realLat : 0,
            realLong: 0,
            accuracy2: '',
            isInPolyGon: '',
            isInPolyGon2: '',
            jsonLog2: '',
      onChangeDistanceFilter: this.onChangeDistanceFilter.bind(this, '')      
    }
    
  }

  render(){
    return (
      <View style={{flex: 1, width: 300, height: 300}} >
        <Modal
            animationType={"slide"}
            transparent={false}
            backdropOpacity={.5}
            onRequestClose={()=>{}}
            visible={!!this.state.isVisible}
        >
          <View style={{flex:1}}>
            <Text style={{alignSelf: 'center' ,color: '#000000', fontSize: 32 }}>Let's Test GeoFencing !!!! </Text>
            
            <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
              <View style={{width: 200, alignSelf: 'flex-start'}}>
                <Button
                onPress={this._onGeoFencingActivate.bind(this)}
                title='Activate GeoFencing Feature'
                />
              </View>
              <View style={{width: 200, alignSelf: 'flex-end'}}>
              <Button
              onPress={this._onCloseClick}
              title='Close GeoFencing Dialog'
               />
            </View>
            </View>  
            
           
            <Text style={{color: '#000000', fontSize: 16}}>
            The Position once get from API that may not changed 
            {'\n'}  
            Original position: {this.state.latitude} , {this.state.longitude}
            {'\n'}
            Accuracy: {this.state.accuracy}
            {'\n'}
            JSON Log : {this.state.jsonLog}
            {'\n'}
            The Point is In area ?? : {this.state.isInPolyGon}

            </Text>
        
            <Text style={{color: '#000000', fontSize: 16}}>
              Continuous / Real-Time Get Current Position Information ::
            </Text>
            
            <Text style={{color: '#000000', fontSize: 16}}>
            Current position: {this.state.realLat} , {this.state.realLong}
            {'\n'}
            Accuracy: {this.state.accuracy2}
            {'\n'}
            JSON Log : {this.state.jsonLog2}
            {'\n'}
            Distance Filter : {this.state.distance}
            {'\n'}
            The Point is In area ?? : {this.state.isInPolyGon2}

            </Text>

            <Text style={{color: '#000000', fontSize: 16}}> Change Distance Filter </Text>
            <TextInput onChangeText={(d)=>this.onChangeDistanceFilter(d)}/>
          </View> 
        </Modal>  
      </View>
    );
  }

  onChangeDistanceFilter = (d) =>{
    this.setState({distance: parseFloat(d == NaN ? 0 : d)})
    navigator.geolocation.clearWatch(this.watchId)
    this.componentDidMount();
  }

  _onCloseClick = () => {
    this.setState({isVisible: false})
    this.state._onGeoFencingCall(false)
  }


  componentDidMount(){
  
    this.watchId = navigator.geolocation.watchPosition(
    (position) => {
      let point = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      this.setState({
          realLat: position.coords.latitude,
          realLong: position.coords.longitude,
          jsonLog2: JSON.stringify(position),
          accuracy2: position.coords.accuracy
      })

      GeoFencing.containsLocation(point, polygon)
        .then(() => this.setState({isInPolyGon2: 'Point is Within Polygon'}))
        .catch(() => this.setState({isInPolyGon2: 'Point is NOT Within Polygon'}))
    },

    (error) => alert(error.message),
    { enableHighAccuracy: true, timeout: 1000, distanceFilter: (this.state.distance == null ? 0 : this.state.distance) }
    );
    }

  // Reset Watch Location to reduce battery  
  componentWillUnmount(){
        navigator.geolocation.clearWatch(this.watchId);
        clearInterval(geoFencingInterval);
  }

   _onGeoFencingActivate = () =>{
    
  geoFencingInterval = setInterval(() =>{
     console.log("Swt Current Position")
      this._getCurrentPosition();
   }, 1000)
    }

    _getCurrentPosition = () =>{
 navigator.geolocation.getCurrentPosition(
    (position) => {
      let point = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          accuracy: position.coords.accuracy,
          jsonLog: JSON.stringify(position)
      })

         GeoFencing.containsLocation(point, polygon)
        .then(() => this.setState({isInPolyGon: 'Point is Within Polygon'}))
        .catch(() => this.setState({isInPolyGon: 'Point is NOT Within Polygon'}))
    },

    (error) => alert(error.message),
    { enableHighAccuracy: true, timeout: 1000 }
    );
    }

}

module.exports = GeoFencingModal;