class Constant {
        static URLs = {
            termAndUseURL: require('../htmls/terms_en.html'),
            privacyURL : require('../htmls/privacy_en.html'),
            internetURL: require('../htmls/internetSecurity_en.html')
        }
        static photoPath = {
            dslalogoLeft : require('../images/dah_sing_logo_left1.png'),
            dslalogo2 : require('../images/dah_sing_logo_left2.png'),
            dslalogoRight: require('../images/dah_sing_logo_right.png'),
            bgLangButton: require('../images/btn_language.9.png'),
            dslasplashLogo : require('../images/dsla_splash.png'),
        }
}

module.exports = Constant;
