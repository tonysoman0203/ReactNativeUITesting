import React, {Component} from 'react';
import {
  AppRegistry,
} from 'react-native';
import { StackNavigator,NavigationActions } from 'react-navigation';

import HeaderScreen from './screens/HeaderScreen'
import FooterScreen from './screens/FooterScreen'
import LoginPage from './screens/LoginPage'
import SplashScreen from './screens/SplashScreen'

const UI_TESTING = StackNavigator({
    SplashScreen: {screen: SplashScreen},
    LoginPage: {screen: LoginPage} },{ 
    headerMode: 'screen'
  }
);


AppRegistry.registerComponent('UI_TESTING', () => UI_TESTING);
